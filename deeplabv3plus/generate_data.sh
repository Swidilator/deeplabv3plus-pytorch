#!/bin/bash

pip install -r requirements.txt
python video_processor.py --num_classes 19 --data_root ./data/cityscapes --dataset cityscapes --test_only --batch_size 4 --ckpt best_deeplabv3plus_mobilenet_cityscapes_os16.pth
