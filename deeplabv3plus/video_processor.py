from tqdm import tqdm
import network
import utils
import os
import random
import argparse
import numpy as np
from PIL import Image

from torch.utils import data
from datasets import VOCSegmentation, Cityscapes
from utils import ext_transforms as et
from metrics import StreamSegMetrics

import torch
import torch.nn as nn


def get_argparser():
    parser = argparse.ArgumentParser()

    # Datset Options
    parser.add_argument(
        "--data_root", type=str, default="./datasets/data", help="path to Dataset"
    )
    parser.add_argument(
        "--dataset",
        type=str,
        default="voc",
        choices=["voc", "cityscapes"],
        help="Name of dataset",
    )
    parser.add_argument(
        "--num_classes", type=int, default=None, help="num classes (default: None)"
    )

    # Deeplab Options
    parser.add_argument(
        "--model",
        type=str,
        default="deeplabv3plus_mobilenet",
        choices=[
            "deeplabv3_resnet50",
            "deeplabv3plus_resnet50",
            "deeplabv3_resnet101",
            "deeplabv3plus_resnet101",
            "deeplabv3_mobilenet",
            "deeplabv3plus_mobilenet",
        ],
        help="model name",
    )
    parser.add_argument(
        "--separable_conv",
        action="store_true",
        default=False,
        help="apply separable conv to decoder and aspp",
    )
    parser.add_argument("--output_stride", type=int, default=16, choices=[8, 16])

    # Train Options
    parser.add_argument("--test_only", action="store_true", default=False)
    parser.add_argument(
        "--save_val_results",
        action="store_true",
        default=False,
        help='save segmentation results to "./results"',
    )
    parser.add_argument(
        "--total_itrs", type=int, default=30e3, help="epoch number (default: 30k)"
    )
    parser.add_argument(
        "--lr", type=float, default=0.01, help="learning rate (default: 0.01)"
    )
    parser.add_argument(
        "--lr_policy",
        type=str,
        default="poly",
        choices=["poly", "step"],
        help="learning rate scheduler policy",
    )
    parser.add_argument("--step_size", type=int, default=10000)
    parser.add_argument(
        "--crop_val",
        action="store_true",
        default=False,
        help="crop validation (default: False)",
    )
    parser.add_argument(
        "--batch_size", type=int, default=16, help="batch size (default: 16)"
    )
    parser.add_argument(
        "--val_batch_size",
        type=int,
        default=4,
        help="batch size for validation (default: 4)",
    )
    parser.add_argument("--crop_size", type=int, default=513)

    parser.add_argument(
        "--ckpt", default=None, type=str, help="restore from checkpoint"
    )
    parser.add_argument("--continue_training", action="store_true", default=False)

    parser.add_argument(
        "--loss_type",
        type=str,
        default="cross_entropy",
        choices=["cross_entropy", "focal_loss"],
        help="loss type (default: False)",
    )
    parser.add_argument("--gpu_id", type=str, default="0", help="GPU ID")
    parser.add_argument(
        "--weight_decay", type=float, default=1e-4, help="weight decay (default: 1e-4)"
    )
    parser.add_argument(
        "--random_seed", type=int, default=1, help="random seed (default: 1)"
    )
    parser.add_argument(
        "--print_interval",
        type=int,
        default=10,
        help="print interval of loss (default: 10)",
    )
    parser.add_argument(
        "--val_interval",
        type=int,
        default=100,
        help="epoch interval for eval (default: 100)",
    )
    parser.add_argument(
        "--download", action="store_true", default=False, help="download datasets"
    )

    # PASCAL VOC Options
    parser.add_argument(
        "--year",
        type=str,
        default="2012",
        choices=["2012_aug", "2012", "2011", "2009", "2008", "2007"],
        help="year of VOC",
    )

    # Visdom options
    parser.add_argument(
        "--enable_vis",
        action="store_true",
        default=False,
        help="use visdom for visualization",
    )
    parser.add_argument("--vis_port", type=str, default="13570", help="port for visdom")
    parser.add_argument("--vis_env", type=str, default="main", help="env for visdom")
    parser.add_argument(
        "--vis_num_samples",
        type=int,
        default=8,
        help="number of samples for visualization (default: 8)",
    )
    return parser


def get_dataset(opts, split: str):
    """ Dataset And Augmentation
    """
    val_transform = et.ExtCompose(
        [
            # et.ExtResize( 512 ),
            et.ExtToTensor(),
            et.ExtNormalize(mean=[0.485, 0.456, 0.406], std=[0.229, 0.224, 0.225]),
        ]
    )

    video_dst = Cityscapes(root=opts.data_root, split=split, transform=val_transform)
    # val_dst = Cityscapes(root=opts.data_root, split="val", transform=val_transform)
    return video_dst


def generate_video_data(model, loader, device, split: str):
    with torch.no_grad():
        for i, (images, _, file_names) in tqdm(enumerate(loader), total=len(loader.dataset) / loader.batch_size):
            print("")
            images = images.to(device, dtype=torch.float32)
            outputs = model(images)
            predictions = outputs.detach().max(dim=1)[1].cpu().numpy()

            for j in range(len(images)):
                img_file_name: str = file_names[j]
                print(f"Processing: {img_file_name}")

                full_folder_path: str = "/root/samba_share/generated_masks/{split}/{folder_name}".format(
                    folder_name=img_file_name.split("_")[0],
                    split=split,
                )
                os.makedirs(full_folder_path, exist_ok=True)

                img_path: str = "{full_folder_path}/{file_name}".format(
                    full_folder_path=full_folder_path,
                    file_name=img_file_name,
                )
                semantic_img_path: str = img_path.replace("leftImg8bit", "gtFine_labelIds")
                colour_img_path = img_path.replace("leftImg8bit", "gtFine_color")

                semantic_img = predictions[j].astype(np.uint8)
                colour_img: np.ndarray = loader.dataset.decode_target(semantic_img)

                a, b, c = np.split(colour_img, 3, axis=2)
                colour_img = np.concatenate((a, b, c), axis=2).astype(np.uint8)
                #bgr_img = bgr_img.transpose((2,0,1))

                colour_img: Image.Image = Image.fromarray(colour_img, mode="RGB")
                colour_img.save(colour_img_path)

                semantic_img: Image.Image = Image.fromarray(semantic_img)
                semantic_img.save(semantic_img_path)

                # bgr_img: np.ndarray = cv2.cvtColor(colour_img, cv2.COLOR_RGB2BGR)

                # result: bool = cv2.imwrite(img_path, bgr_img)


                # if not result:
                #     raise ValueError


def main():
    opts = get_argparser().parse_args()
    if opts.dataset.lower() == "voc":
        opts.num_classes = 21
    elif opts.dataset.lower() == "cityscapes":
        opts.num_classes = 19

    os.environ["CUDA_VISIBLE_DEVICES"] = opts.gpu_id
    device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
    print("Device: %s" % device)

    # Setup random seed
    torch.manual_seed(opts.random_seed)
    np.random.seed(opts.random_seed)
    random.seed(opts.random_seed)

    # Set up model
    model_map = {
        "deeplabv3_resnet50": network.deeplabv3_resnet50,
        "deeplabv3plus_resnet50": network.deeplabv3plus_resnet50,
        "deeplabv3_resnet101": network.deeplabv3_resnet101,
        "deeplabv3plus_resnet101": network.deeplabv3plus_resnet101,
        "deeplabv3_mobilenet": network.deeplabv3_mobilenet,
        "deeplabv3plus_mobilenet": network.deeplabv3plus_mobilenet,
    }

    model = model_map[opts.model](
        num_classes=opts.num_classes, output_stride=opts.output_stride
    )
    if opts.separable_conv and "plus" in opts.model:
        network.convert_to_separable_conv(model.classifier)
    utils.set_bn_momentum(model.backbone, momentum=0.01)

    # Restore
    if opts.ckpt is not None and os.path.isfile(opts.ckpt):
        checkpoint = torch.load(opts.ckpt)
        model.load_state_dict(checkpoint["model_state"])
        print("Model restored from %s" % opts.ckpt)
        del checkpoint  # free memory
    else:
        print("[!] Retrain")

    model = nn.DataParallel(model)
    model.to(device)

    # ==========   Train Loop   ==========#
    # denorm = utils.Denormalize(
    #     mean=[0.485, 0.456, 0.406], std=[0.229, 0.224, 0.225]
    # )  # denormalization for ori images

    model.eval()


    # Setup dataloader
    if opts.dataset == "voc" and not opts.crop_val:
        opts.val_batch_size = 1

    # Split for which data to process
    split: str = "val"

    video_dst = get_dataset(opts, split)
    video_loader = data.DataLoader(
        video_dst, batch_size=opts.val_batch_size, shuffle=False, num_workers=2
    )
    print("Dataset: %s, Video Set: %d" % (opts.dataset, len(video_dst)))

    # Generate video data
    generate_video_data(model=model, loader=video_loader, device=device, split=split)


if __name__ == "__main__":
    main()
